<?php

namespace kfit\tools\controllers;

use Yii;
use kfit\core\base\Controller;

/**
 * Controlador DashboardController
 *
 * @package kfit\tools\controllers\DashboardController
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S. 
 *
 */
class DashboardController extends Controller
{

    public function actionUpdateSecuences()
    {
        Yii::$app->dbHelper->updateSequences();
        return $this->redirect(['index']);
    }
}
