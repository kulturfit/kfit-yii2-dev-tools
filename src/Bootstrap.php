<?php

namespace kfit\tools;

use yii\base\Application;
use kfit\core\base\Bootstrap as BootstrapBase;
use Yii;

/**
 * Clase cargadora de características para la aplicación.
 *
 * @package kfit\tools\Booststrap
 * 
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com @version 0.0.1
 * @since 0.0.0
 */
class Bootstrap extends BootstrapBase
{
    public $moduleId = 'tools';
    public $pieces = [
        'modules' => [
            'tools' => \kfit\tools\Module::class
        ],
        'components' => [
            'dbHelper' => \kfit\tools\helpers\DbToolsHelper::class,
        ]
    ];

    /**
     * Bootstrap method to be called during application bootstrap stage.
     *
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {
        parent::bootstrap($app);

        Yii::$app->getAssetManager()->publish('@kfit/tools/assets');

        if ($app instanceof \yii\console\Application) {
            $app->getModule($this->moduleId)->controllerNamespace =
                'kfit\tools\commands';
        }
    }
}
