<?php

namespace kfit\tools\helpers;

use Yii;

/**
 * 
 * @package kfit\tools\helpers\DbToolsHelper
 *
 * @author  Daniel Julian Sanchez Alvarez <daniel.sanchez@timakers.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class DbToolsHelper extends \yii\base\Component
{

    /**
     * Undocumented function
     *
     * @param yii\db\Connection $db
     * @return void
     */
    public function updateSequences($db =  null)
    {
        $result = true;

        if (empty($db)) {
            $db = Yii::$app->getDb();
        }

        try {
            $sql = "
        SELECT 'SELECT SETVAL(' ||
            quote_literal(quote_ident(PGT.schemaname) || '.' || quote_ident(S.relname)) ||
            '::regclass, COALESCE(MAX(' ||quote_ident(C.attname)|| '), 1)::bigint, true ) FROM ' ||
            quote_ident(PGT.schemaname)|| '.'||quote_ident(T.relname)|| ';' as query
        FROM pg_class AS S,
            pg_depend AS D,
            pg_class AS T,
            pg_attribute AS C,
            pg_tables AS PGT
        WHERE S.relkind = 'S'
            AND S.oid = D.objid
            AND D.refobjid = T.oid
            AND D.refobjid = C.attrelid
            AND D.refobjsubid = C.attnum
            AND T.relname = PGT.tablename
        ORDER BY S.relname;
        ";

            $scripts = $db->createCommand($sql)->queryAll();
            $scripts = Yii::$app->arrayHelper::getColumn($scripts, 'query');

            foreach ($scripts as $script) {
                $db->createCommand($script)->execute();
            }
        } catch (\Throwable $th) {
            //throw $th;
            $result = false;
        }
        return $result;
    }
}
