<?php
return [
    'Update secuences' => 'Actualizar secuencias',
    'When a data migration has been performed or inserted directly into the tables without using the sequences properly, this tool can be used to update all sequences associated with tables of the active connection' => 'Cuando se ha realizado una migración de datos o se han insertado directamente en las tablas sin utilizar las secuencias de forma apropiada, se puede utilizar esta herramienta para actualizar todas las secuencias asociadas a tablas de la conexión activa Yii::$app->getDb().',
    'Source: {url}' => 'Fuente: {url}',
    'Run' => 'Ejecutar'
];
