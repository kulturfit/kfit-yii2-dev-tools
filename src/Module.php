<?php

namespace kfit\tools;

use Yii;

/**
 * Base module definition class
 */
class Module extends \kfit\core\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $defaultRoute = 'dashboard';

    /**
     * Permite establecer las migas de pan base para el módulo
     *
     * @var array
     */
    public $breadcrumbsBase;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        if (empty($this->breadcrumbsBase)) {
            $this->breadcrumbsBase = [];
        }
        // inicializa el módulo con la configuración cargada desde config.php
        \Yii::configure($this, require __DIR__ . '/config/main.php');
        $this->registerTranslations();
    }

    /**
     * Registra las traducciones para los mensajes de todo el módulo
     *
     * @return void
     */
    public function registerTranslations()
    {
        if (!isset(Yii::$app->i18n->translations["{$this->id}*"])) {
            Yii::$app->i18n->translations["{$this->id}*"] = [
                'class' => 'yii\i18n\PhpMessageSource',
                'sourceLanguage' => 'en-US',
                'basePath' => __DIR__ . '/messages',
                'fileMap' => [
                    $this->id => 'app.php',
                ],
            ];
        }
    }
}
