<?php

use yii\helpers\Url;
?>
<div class="row">
    <div class="col-12">
        <div class="d-flex justify-content-around flex-wrap">
            <div class="card mb-3" style="max-width: 540px;">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="<?= Yii::$app->getAssetManager()->getPublishedUrl('@kfit/tools/assets') . '/images/data.jpg' ?>" class="card-img h-100" alt="<?= Yii::t('tools', 'Update secuences') ?>" style="object-fit: cover;">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title"><?= Yii::t('tools', 'Update secuences') ?></h5>
                            <p class="card-text">
                                <?= Yii::t('tools', 'When a data migration has been performed or inserted directly into the tables without using the sequences properly, this tool can be used to update all sequences associated with tables of the active connection') ?></p>
                            <?= Yii::$app->html::a(Yii::t('tools', 'Run'), ['update-secuences'], ['class' => 'btn btn-primary w-100 mb-20']) ?>
                            <p class="card-text"><small class="text-muted"><?= Yii::t('tools', 'Source: {url}', ['url' => Url::to('https://wiki.postgresql.org/wiki/Fixing_Sequences')]) ?></small></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>